 function showHideDescription() {
     $(this).find(".description").toggle();
}

var prixType = 0;
var prixPate = 0;
var prixExtra = 0;
var nbParts = 1;

 function affichePrix(prix, choix) {
	switch(choix) {
		case "type":
			prixType = prix*nbParts/6;
			break;
			
		case "pate":
			prixPate = prix;
			break;
			
		case "extra":
			prixExtra = prix;
			break;
	}
	total = prixType + prixExtra + prixPate;
	$(".tile p").html(total + " €");
 }
 
 function test() {
	 total = 5;
 }

$(document).ready(function(){
    $('.pizza-type label').hover(
    showHideDescription,
    showHideDescription
    );

    $('.nb-parts input').on('keyup', function() {
        $(".pizza-pict").remove();
        var pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();
		nbParts = slices; 
		
        for(i = 0; i < slices/6 ; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);
    });

	$('.next-step').click(function() {
		$('.infos-client').show();
		$('.next-step').hide();
        
	});

    $('.add').click(function() {
	    $(".infos-client .add").before("<br><input type='text'/>");
    });

    $('.done').click(function() {
	    $("body").hide();
        var prenom = $(".infos-client .type:first input").val();
		document.write("Merci "+ prenom + " ! Votre commande sera livrée dans 15 minutes.");
    });
	
	$('.pizza-type').click(function() {
		var prix = $('input[name=type]:checked').data('price');
		affichePrix(prix, "type");
	});
	
	
	$('input[name=pate]').click(function() {
		var prix = $('input[name=pate]:checked').data('price');
		affichePrix(prix, "pate");
	});
	
	$('input[name=extra]').click(function() {
		var prix = 0;
		$('input[name=extra]:checked').each(function() { 
			prix += $(this).data('price'); 
		});
		affichePrix(prix, "extra");
	});
    
});


